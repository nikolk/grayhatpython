from ctypes import *
from my_debugger_defines import *
from __builtin__ import False, True
from symbol import continue_stmt
from test.test_largefile import size
from pickle import FALSE

kernel32 = windll.kernel32

class debugger():
    def __init__(self):
        self.h_process = None
        self.pid = None
        self.debugger_active = False
        self.h_thread = None
        self.context = None
        self.exception = None
        self.exception_address = None
        self.breakpoints = {}
        self.first_breakpoint = True
        self.hardware_breakpoints = {}
        
        # get default page size
        system_info = SYSTEM_INFO()
        kernel32.GetSystemInfo(byref(system_info))
        self.page_size = system_info.dwPageSize
        
        self.guarded_pages = []
        self.memory_breakpoints = {}
        
        
        
    
    def load(self, path_to_exe):
        creation_flags = DEBUG_PROCESS
        # create structs
        startupinfo = STARTUPINFO()
        process_information = PROCESS_INFORMATION()
        
        # options of the started process
        startupinfo.dwFlags = 0x1
        startupinfo.wShowWindow = 0x0
        startupinfo.cb = sizeof(startupinfo)
        
        # create process
        if kernel32.CreateProcessA(path_to_exe, None, None, None, None, creation_flags, None, None, byref(startupinfo), byref(process_information)):
            print "[*] We have successfully launched  the process!"
            print "[*] PID: %d" % process_information.dwProcessId
            # obtain handler for process
            self.h_process = self.open_process(process_information.dwProcessId)
        else:
            print "[*] Error: 0x%08x." % kernel32.GetLastError()

        
    # this function returns handler for process
    def open_process(self, pid):
        h_process = kernel32.OpenProcess(PROCESS_ALL_ACCESS, False, pid)
        print("error code: " + str(kernel32.GetLastError()))
        return h_process
    
    def attach(self, pid):
        self.h_process = self.open_process(pid)
        # attempt to attach to the process
        if kernel32.DebugActiveProcess(DWORD(pid)):
            self.debugger_active = True
            self.pid = int(pid)
            self.run()
        else:
            print "[*] Unable to attach to the process"
            print("error code: " + str(kernel32.GetLastError()))
            
    def run(self):
        while self.debugger_active:
            self.get_debug_event()
            
            
    def get_debug_event(self):
        debug_event = DEBUG_EVENT()
        continue_status = DBG_CONTINUE
        
        if kernel32.WaitForDebugEvent(byref(debug_event), INFINITE):
            # obtain thread and context info
            self.h_thread = self.open_thread(debug_event.dwThreadId)
            self.context = self.get_thread_context(self.h_thread)
            
            print "Event Code: %d Thread ID: %d" % (debug_event.dwDebugEventCode, debug_event.dwThreadId)
            
            # if the event code is exception, we want to examine it further
            if debug_event.dwDebugEventCode == EXCEPTION_DEBUG_EVENT:
                # exception code
                exception = debug_event.u.Exception.ExceptionRecord.ExceptionCode
                self.exception_address = debug_event.u.Exception.ExceptionRecord.ExceptionAddress
                
            if exception == EXCEPTION_ACCESS_VIOLATION:
                print "Access Violation Detected."
                
            # if breakpoint is detected
            elif exception == EXCEPTION_BREAKPOINT:
                continue_status = self.exception_handler_breakpoint()
                
            elif exception == EXCEPTION_GUARD_PAGE:
                print "Guard Page Access Detected."
                
            elif exception == EXCEPTION_SINGLE_STEP:
                self.exception_handler_single_step()
            
            kernel32.ContinueDebugEvent(debug_event.dwProcessId, debug_event.dwThreadId, continue_status)
    
    
    def exception_handler_breakpoint(self):
        print "[*] Inside the breakpoint handler."
        print "Exception Address: 0x%08x" % self.exception_address
        return DBG_CONTINUE
    
    def exception_handler_single_step(self):
        if self.context.Dr6 & 0x1 and self.hardware_breakpoints.has_key(0):
            slot = 0
        elif self.context.Dr6 & 0x2 and self.hardware_breakpoints.has_key(1):
            slot = 1
        elif self.context.Dr6 & 0x4 and self.hardware_breakpoints.has_key(2):
            slot = 2
        elif self.context.Dr6 & 0x8 and self.hardware_breakpoints.has_key(3):
            slot = 3
        else:
            # this wasn't hardware breakpoint
            continue_status = DBG_EXCEPTION_NOT_HANDLED
        
        # remove breakpoint from the list
        if self.bp_del_hw(slot):
            continue_status = DBG_CONTINUE
        print "[*] Hardware breakpoint removed."
        return continue_status
            
    
    def detach(self):
        if kernel32.DebugActiveProcessStop(self.pid):
            print "[*] finished debugging. Exiting.... "
            return True
        else:
            print "There was an error"
            return False
        
    def open_thread(self, thread_id):
        h_thread = kernel32.OpenThread(THREAD_ALL_ACCESS, None, thread_id)
        
        if h_thread is not None:
            return h_thread
        else:
            print "[*] Could not obtain a valid thread handler"
            return False
        
    def enumerate_threads(self):
        thread_entry = THREADENTRY32()
        thread_list = []
        
        snapshot = kernel32.CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, self.pid)

        if snapshot is not None:
            # must set the size otherwise the call will fail
            thread_entry.dwSize = sizeof(thread_entry)
            success = kernel32.Thread32First(snapshot, byref(thread_entry))

            while success:
                if thread_entry.th32OwnerProcessID == self.pid:
                    thread_list.append(thread_entry.th32ThreadID)
                success = kernel32.Thread32Next(snapshot, byref(thread_entry))

            kernel32.CloseHandle(snapshot)
            return thread_list
        
        else:
            return False
        
    def get_thread_context(self, thread_id):
        context = CONTEXT()
        context.ContextFlags = CONTEXT_FULL | CONTEXT_DEBUG_REGISTERS

        # obtain handle to thread
        h_thread = self.open_thread(thread_id)
        if kernel32.GetThreadContext(h_thread, byref(context)):
            kernel32.CloseHandle(h_thread)
            return context
        else:
            return False
        
    def read_process_memory(self, address, length):
        data = ""
        read_buf = create_string_buffer(length)
        count = c_ulong(0)
        
        if not kernel32.ReadProcessMemory(self.h_process, address, read_buf, length, byref(count)):
            return False
        else:
            data += read_buf.raw
            return data
        
    def write_process_memory(self, address, data):
        count = c_ulong(0)
        length = len(data)
        
        c_data = c_char_p(data[count.value:])
        
        if not kernel32.WriteProcessMemory(self.h_process, address, c_data, length, byref(count)):
            return False
        else:
            return True
        
    def bp_set(self, address):
        # ------ soft breakpoints -------
        if not self.breakpoints.has_key(address):
            try:
                # store the original byte
                original_byte = self.read_process_memory(address, 1)
                # write instead the int3 opcode
                self.write_process_memory(address, "\xCC")
                # register the breakpoint
                self.breakpoints[address] = (address, original_byte) 
            except:
                return False
            
        return True
    
    def func_resolve(self, dll, function):
        handle = kernel32.GetModuleHandleA(dll)
        address = kernel32.GetProcAddress(handle, function)
        
        kernel32.CloseHandle(handle)
        return address 
    
    def bp_set_hw(self, address, length, condition):
        # check valid length value
        if length not in (1, 2, 4):
            return False
        else:
            length -= 1
            
        # check for a valid condition
        if condition not in (HW_ACCESS, HW_EXECUTE, HW_WRITE):
            return False
        
        # check for available slots
        if not self.hardware_breakpoints.has_key(0):
            available = 0
        elif not self.hardware_breakpoints.has_key(1):
            available = 1
        elif not self.hardware_breakpoints.has_key(2):
            available = 2
        elif not self.hardware_breakpoints.has_key(3):
            available = 3
        else:
            return False
        
        # set debug register in every thread in the process
        for theard_id in self.enumerate_threads():
            context = self.get_thread_context(theard_id)
            
            context.Dr7 |= 1 << (available)  # set the breakpoint
            
        # save the address in available slot
        if available == 0:
            context.Dr0 = address
        elif available == 1:
            context.Dr1 = address
        elif available == 2:
            context.Dr2 = address
        elif available == 3:
            context.Dr3 = address
            
        # set the breakpoint condition
        context.Dr7 |= condition << ((available * 4) + 16)
        
        # set the length
        context.Dr7 |= length << ((available * 4) + 18)
        
        self.h_thread = self.open_thread(theard_id)
        kernel32.SetThreadContext(self.h_thread, byref(context))
        
        self.hardware_breakpoints[available] = (address, length, condition)
        return True
    
    
    def bp_del_hw(self, slot):
        # disable breakpoint for all the active threads
        for thread_id in self.enumerate_threads():
            context = self.get_thread_context(thread_id)
            
            # reset the flags to remove breakpoint
            context.Dr7 &= ~(1 << (slot * 2))
            
            # zero out the address
            if slot == 0:
                context.Dr0 = 0
            elif slot == 1:
                context.Dr1 = 0
            elif slot == 2:
                context.Dr2 = 0
            elif slot == 3:
                context.Dr3 = 0
                
            # remove the condition flag
            context.Dr7 &= ~(3 << ((slot * 4) + 16))
            
            # remove the length flag
            context.Dr7 &= ~(3 << ((slot * 4) + 18))
            
            # reset the thread's context
            h_thread = self.open_thread(thread_id)
            kernel32.SetThreadContext(h_thread, byref(context))
            
        # remove breakpoint from the list
        del self.hardware_breakpoints[slot]
        return True
            
    def bp_set_mem(self, address, size):
        mbi = MEMORY_BASIC_INFORMATION()
        
        if kernel32.VirtualQueryEx(self.h_process, address, byref(mbi), sizeof(mbi)) < sizeof(mbi):
            return False
            
        current_page = mbi.BaseAddress
        
        # set permissions on all pages that are affected by the memory breakpoint
        while current_page <= address + size:
            # add page to list
            self.guarded_pages.append(current_page)
            
            old_protection = c_ulong(0)
            # set the permission on the page
            if not kernel32.VirtualProtectEx(self.h_process, current_page, size, mbi.Protect | PAGE_GUARD, byref(old_protection)):
                return False
            
            # next page
            current_page += self.page_size
            
        # add memory breakpoint to list
        self.memory_breakpoints[address] = (address, size, mbi)
        
        # when memory breakpoint is reached the operating system removes it - that's why handle for removing memory breakpoint is not needed
        return True
            
            
            
            
            
    
    
    